package itb.cat.ex2;

public class DeixarDeFumar {
    private static Thread t1, t2;
    public static void main(String[] args) {
        Runnable r1=()->fumador();
        Runnable r2=()->metge();
        t1=new Thread(r1);
        t2=new Thread(r2);
        t1.start();
        t2.start();
    }

    public static void fumador() {
        int i = 0;
        while (!t1.isInterrupted()) {
            System.out.println("Fumo cigarreta... " + i);
            i++;
        }
        System.out.println("STOP");
    }

    public static void metge() {
        try { //pauso 3 segons i després interrompeixo el primer thread
            Thread.sleep(3000);
            t1.interrupt();
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
}
