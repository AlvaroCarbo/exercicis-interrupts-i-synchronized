package itb.cat.ex1;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Alarma {
    private static Thread ovelles;
    private static Thread alarma;

    public static void main(String[] args) {
        Runnable runner1 = () -> comptar();
        ovelles = new Thread(runner1);
        ovelles.start();

        Runnable runner2 = () -> interrompre();
        alarma = new Thread(runner2);
        alarma.start();
    }

    /* el primer thread només fa que comptar indefinidament d'1 en 1, pausa 1 segon a cada ronda*/
    public static void comptar() {
        LocalTime inici = LocalTime.now();
        try {
            int i = 1;
            while (true) {
                System.out.println("Ovella " + i);
                i++;
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            long temps = Duration.between(inici,LocalTime.now()).getSeconds();
            System.out.println("Bon dia. Has dormit: " + temps + " segons.");
        }
    }

    public static void interrompre() {
        try { //pauso 3 segons i després interrompeixo el primer thread
            Thread.sleep(3000);
            ovelles.interrupt();
        } catch (InterruptedException e) {
            System.out.println(e);
        }

    }
}
