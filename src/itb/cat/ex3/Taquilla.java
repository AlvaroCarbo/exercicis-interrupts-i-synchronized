package itb.cat.ex3;

public class Taquilla  {
    private int entrades = 10;

    public  void reservarEntrada(String nomUsuari, int numEntrades) {
        synchronized (this) {
            if (numEntrades > 4) {
                System.out.println("Ho sentim " + nomUsuari + " però, el número màxim d'entrades per persona que es pot solicitar és 4. Entrades solicitades: " + numEntrades);
            } else {
                if (entrades - numEntrades >= 0){
                    entrades -= numEntrades;
                    System.out.println("Usuari: " + nomUsuari + " a comprat " + numEntrades + " entrada/es. Entrades restants: " + getValue());
                } else {
                    System.out.println("Ho sentim " + nomUsuari + ". Pel moment no queden suficients entrades disponibles.\n" + "Entrades solitades: " + numEntrades + " Entrades disponibles: " + getValue());
                }
            }

            System.out.println("-----------------------------");
        }
    }

    public  int getValue() {
        synchronized (this) {
            return entrades;
        }
    }
}

class RaceConditionProva{
    public static void main(String[] args) {
        Taquilla taquilla  = new Taquilla();
        Runnable persona1 =()->{
            String nom = "Josep";
            int entradesSolicitades = (int)(Math.random() * 5) + 1;
            taquilla.reservarEntrada(nom, entradesSolicitades);
        };
        Runnable persona2 =()->{
            String nom = "Pau";
            int entradesSolicitades = (int)(Math.random() * 5) + 1;
            taquilla.reservarEntrada(nom, entradesSolicitades);
        };
        Runnable persona3 =()->{
            String nom = "Joan";
            int entradesSolicitades = (int)(Math.random() * 5) + 1;
            taquilla.reservarEntrada(nom, entradesSolicitades);
        };
        Runnable persona4 =()->{
            String nom = "Pere";
            int entradesSolicitades = (int)(Math.random() * 5) + 1;
            taquilla.reservarEntrada(nom, entradesSolicitades);
        };
        Runnable persona5 =()->{
            String nom = "Miquel";
            int entradesSolicitades = (int)(Math.random() * 5) + 1;
            taquilla.reservarEntrada(nom, entradesSolicitades);
        };
        new Thread(persona1).start();
        new Thread(persona2).start();
        new Thread(persona3).start();
        new Thread(persona4).start();
        new Thread(persona5).start();
    }
}