### Exercici 1: Alarma
***Interrupt***

---

Quan una persona se’n va a dormir intenta comptar ovelles per agafar la son. Al mateix temps, posa l’alarma del dia següent, per aixecar-se. Simuleu aquest procés amb dos threads, un que compta ovelles de manera ininterrompuda (mostreu un missatge per consola amb el número d’ovella i espereu un segon) i un altre thread que, en background, esperarà fins que sigui l’hora de despertar-se. Quan arribi aquest moment, envieu una interrupció al thread que compta ovelles, per tal de despertar-se i acabar el thread. Utilitzeu les classes LocalTime i Duration pel codi associat a temps.

---


### Exercici 2: DeixarDeFumar
***Interrupt***

---
```
public class DeixarDeFumar {
   private static Thread t1, t2;
   public static void main(String[] args) {
       Runnable r1=()->fumador();
       Runnable r2=()->metge();
       t1=new Thread(r1);
       t2=new Thread(r2);
       t1.start();
       t2.start();
   }
```

Afegiu el què falta dins la classe DeixarDeFumar per aconseguir el següent funcionament:

- El fumador mostra el missatge “Fumo cigarreta...”+ un comptador que comença en 1 i es va incrementant d’un en un de manera repetida.
- El metge interrompeix el thread t1 passats 3 segons després de començar.
- El fumador fuma sense parar (no té cap sleep al seu codi)

---

### Exercici 3: Venda d’entrades
***Synchronized***

---

Es vol simular la venda d’entrades per un concert d’una artista amb molt d’èxit (per exemple, Rosalía). Les entrades es compren a una taquilla virtual que emmagatzema la quantitat màxima d’entrades a vendre (per exemple, 10 entrades, per poder comprovar casos de falta de disponibilitat). Cada usuari (del qual només necessitem el nom) intenta reservar entrades en aquesta taquilla. A la classe Taquilla tindreu el mètode:

`
void reservarEntrades (String nomUsuari, int numEntrades);
`

El funcionament principal consisteix en posar en marxa tants threads com usuaris volen comprar entrades (per exemple, per simplificar,  5 persones). Si intenta reservar més de 4 entrades, el sistema li dirà (per consola) que no pot comprar més de 4 entrades i perd el torn (s’acaba el thread). Si queden suficients entrades, li direm a l’usuari que les hem reservat satisfactòriament i si ja no queden en el moment de reservar mostrarem el missatge d’exhaurides. Acompanyeu tots aquests missatges amb el nom de l’usuari que intenta reservar-les. Heu d’utilitzar recursos de sincronització de fils per evitar vendre més entrades de les disponibles.
